# Programari per al control de versions: GIT


### Instal·lació del programari

Podríem comprovar si tenim instal·lat git i, si no és el cas, l'instal·lar-lo,
però optarem millor per ordenar al sistema que l'instal·li i ja s'encarregarà ell de dir-nos si ho fa o no:

  ```
  [root@paradise ~]# dnf install git
  ```


## Inicialització del repositori

Primer de tot escollim el directori del qual volem fer el seguiment (tracking)
de manera que tindrem un històric de tots els canvis que hi haurà en aquest directori.

Un cop escollit un que ja existeix, o un de nou, ens col·loquem al directori.
Tot i que no seria necessari posem aquest fitxer que esteu llegint al directori esmentat.

Posteriorment inicialitzem el repositori amb l'ordre:

  ```
  git init
  ```

I ens fixem en el missatge que retorna la instrucció anterior.

  ```
  "Initalized an empty git repository in /home/.../el_directori_escollit/.git"
  ```

  El missatge és interessant: tot i que al menys hi ha un fitxer, diu que el repository git està buit.

## Mostra de l'estat del repositori

Hi ha una ordre de git que ens permet comprovar l'estat del repositori:

  ```
  git status
  ```
que ens mostra:

  ```
  ...
  Untracked files:
    (use "git add <file>..." to include in what will be committed)

	   primers_passos.txt      # <==== o el fitxer o fitxers que hi hagi
     nothing added to commit but untracked files present (use "git add" to track)
  ...
  ```


És a dir ens diu bàsicament que:

- Hi ha algun/s fitxer/s dintre del directori del/s qual/s no estem fent seguiment (tracking)
- Si volem fer el seguiment del/s fitxer/s ens dona la instrucció per fer-ho.

Abans però farem un cop d'ull a l'esquema d'un projecte git que es troba en [un altre fitxer](esquema_projecte_git.md).
