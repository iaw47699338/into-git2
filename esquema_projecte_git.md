Un projecte git el podem esquematitzar amb les següents parts
- Un directori de treball: a on farem tot el treball editant, creant, esborrant i organitzant els fitxers.
- Una àrea temporal: a on s'inclouran els canvis fets al directori de treball
- Un repositori: a on Git emmagatzemarà permanentment els canvis anteriors com a versions diferents del mateix projecte

![Esquema d'un projecte GIT](esquema_projecte_git.png) 

Amb quines instruccions es treballar en cada element?
- Per al primer no cal explicar res.
- Per al segon amb l'ordre git add
- Per al tercer amb l'ordre git commit

Anem a treballar-les

