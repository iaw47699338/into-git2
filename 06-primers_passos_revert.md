## Revertir un canvi

Hi ha diferents maneres de desfer certs canvis, algunes més arriscades que d'altres.
Ara ja no estem parlant de canvis abans de fer el *commit*, recordem que per aquests casos existeix l'ordre `checkout` per tornar a tenir al directori de treball la versió que hi ha a l'índex de qualsevol fitxer i `checkout HEAD` per tornar a la darrera versió del *commit*.

#### Problema: el darrer commit (versió) no ens agrada i volem anara a una versió anterior.

Podem fer-ho de diferents maneres.

* Una opció que és poc recomanable és **reset**:
	```
	git reset
	```
	Aquesta ordre elimina el darrer *commit* i ens actualitza al penúltim *commit* (en realitat fem que *HEAD* apunti al penúltim *commit* en comptes de l'últim, el qual eliminem). Aquesta opció és molt poc recomanable quan es treballa en equip per raons òbvies.

* L'opció que farem servir copia la penúltima opció després de la última:
	```
	git revert [6c7ff97d9e88ab88c02ff89362cc3e1f361af71a]
	```

	La manera de fer-la servir és ajudant-se de l'ordre `git log` primer i fent un `git revert id` on *id* és l'identificador del *commit* al qual volem arribar.
